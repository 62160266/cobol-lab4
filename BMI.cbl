       IDENTIFICATION DIVISION.
       PROGRAM-ID. BMI.
       AUTHOR. THUN WITYAKON.
       DATA DIVISION.
       WORKING-STORAGE SECTION.
       01  HEIGHT   PIC 999.
       01  WEIGHT   PIC 9(2).
       01  BMI      PIC 99V99.
       PROCEDURE DIVISION.
       BEGIN.
           DISPLAY "Please input your Height (CM)"
           ACCEPT HEIGHT 
           DISPLAY "Please input your Weight (KK)"
           ACCEPT WEIGHT 
           COMPUTE BMI = WEIGHT / ((HEIGHT/100) * 2) 
           DISPLAY "Your BMI is " BMI 
           IF BMI < 18.5 THEN
              DISPLAY "underweight"
           END-IF 

           IF BMI >= 18.5 AND < 24.9 THEN
              DISPLAY "normal"
           END-IF

           IF BMI >= 25 AND < 29.9 THEN
              DISPLAY "overweight"
           END-IF

           IF BMI > 30 THEN
              DISPLAY "obese"
           END-IF

           .